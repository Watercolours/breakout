(function() {
    var canvas = document.getElementById('breakout');
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    var ctx = canvas.getContext('2d');

    var score = 0;
    var lives = 3;

    var x = canvas.width / 2;
    var y = canvas.height - 30;

    var dx = 5;
    var dy = -5;

    var ballRad = 10;

    var paddleHeight = 10;
    var paddleWidth = 75;
    var paddleX = (canvas.width - paddleWidth) / 2;

    var rightPressed = false;
    var leftPressed = false;

    var brickRows = 5;
    var brickColumns = 10;
    var brickHeight = 30;
    var brickPadding = 0;
    var brickOffsetTop = 80;
    var brickOffsetLeft = 0;
    var brickWidth = (canvas.width / brickColumns) - brickPadding;

    var powerUpFalling = false;
    var powerUp = { x: 0, y: 0, power: null, dy: 5 };

    var bricks = [];
    for (var c = 0; c < brickColumns; c++) {
        bricks[c] = [];
        for (var r = 0; r < brickRows; r++) {
            bricks[c][r] = { x: 0, y: 0, status: 1 };
        }
    }

    document.addEventListener('keydown', keyDownHandler, false);
    document.addEventListener('keyup', keyUpHandler, false);
    document.addEventListener('mousemove', mouseMoveHandler, false);

    function draw() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        createBall();
        drawPaddle();
        drawBricks();
        drawScore();
        drawLives();
        checkCollision();
        checkPlayerInput();
        if (powerUpFalling) {
            drawPowerUp();
        }
        x += dx;
        y += dy;
        requestAnimationFrame(draw);
    }

    function createBall() {
        ctx.beginPath();
        ctx.arc(x, y, ballRad, 0, Math.PI * 2, false);
        ctx.fillStyle = 'white';
        ctx.fill();
        ctx.closePath();
    }

    function drawPaddle() {
        ctx.beginPath();
        ctx.rect(paddleX, canvas.height - paddleHeight, paddleWidth, paddleHeight);
        ctx.fillStyle = 'white';
        ctx.fill();
        ctx.closePath();
    }

    function drawBricks() {
        for (c = 0; c < brickColumns; c++) {
            for (r=0; r < brickRows; r++) {
                if (bricks[c][r].status == 1) {
                    var bx = (c*(brickWidth)) + brickOffsetLeft;
                    var by = (r*(brickHeight)) + brickOffsetTop;
                    bricks[c][r].x = bx;
                    bricks[c][r].y = by;

                    ctx.beginPath();
                    ctx.rect(bx,by, brickWidth, brickHeight);
                    ctx.fillStyle = 'white';
                    ctx.fill();
                    ctx.strokeStyle = 'black';
                    ctx.stroke();
                    ctx.closePath();
                }
            }
        }
    }

    function drawPowerUp() {
        ctx.beginPath();
        ctx.arc(powerUp.x, powerUp.y, 10, 0, Math.PI * 2, false);
        ctx.fillStyle = 'cyan';
        ctx.fill();
        ctx.closePath();
        powerUp.y += powerUp.dy;
    }

    function drawScore() {
        ctx.font = '32px monospace';
        ctx.fillStyle = 'white';
        ctx.fillText('Score: ' + score, canvas.width - 200, 32);
    }

    function drawLives() {
        ctx.font = '32px monospace';
        ctx.fillStyle = 'white';
        var lifeString = "";
        for (var i=0; i < lives; i++) {
            lifeString+= " <3";
        }
        ctx.fillText(lifeString, 0, 32);
    }

    function checkPlayerInput() {
        if (rightPressed) {
            paddleX += 7;
        } else if (leftPressed) {
            paddleX -= 7;
        }
    }

    function checkCollision() {
        // Ball hits walls or ceiling
        if (x + dx < ballRad || x + dx > (canvas.width - ballRad)) {
            dx = -dx;
        }
        if (y + dy < ballRad) {
            dy = -dy;
        }

        if(y + dy > canvas.height - ballRad) {
            if (lives > 0) {
                dy = -dy;
                x = paddleX + paddleWidth / 2;
                y = canvas.height - (paddleHeight + ballRad);
                lives--;
            } else {
                alert('Game Over!');
                document.location.reload();
            }
        }
        // Ball hits paddle
        if (y > (canvas.height - ballRad - paddleHeight) && (x > paddleX && x < paddleX + paddleWidth)) {
            dy = -dy;
        }
        // Ball hits bricks
        for (c=0; c< brickColumns; c++) {
            for (r=0; r < brickRows; r++) {
                var b = bricks[c][r];
                if (b.status == 1) {
                    if(x > b.x && x < b.x+brickWidth && (y < b.y + brickHeight + ballRad)) {
                        dy = -dy;
                        b.status = 0;
                        score++;
                        if (Math.random() > 0.0001) {
                            powerUpFalling = true;
                            powerUp.x = b.x + brickWidth / 2;
                            powerUp.y = b.y + brickHeight / 2;
                            powerUp.power = Math.random();
                        }
                        if (score == brickColumns*brickRows) {
                            alert('You win!');
                            document.location.reload();
                        }
                        break;
                    }
                }
            }
        }
        // Power Up hits paddle
        if (powerUpFalling) {
            if (powerUp.y > (canvas.height - ballRad - paddleHeight) &&
                (powerUp.x > paddleX && powerUp.x < paddleX + paddleWidth)) {
                powerUpFalling = false;
                powerUp.x = 0;
                powerUp.y = 0;
                paddleWidth += 20;
            }
        }
    }

    /*
    * Controllers
     */
    function mouseMoveHandler(e) {
        var relX = e.clientX - canvas.offsetLeft;
        if (relX > 0 && relX < canvas.width) {
            paddleX = relX - paddleWidth / 2;
        }
    }

    function keyDownHandler(e) {
        if (e.keyCode == 39) {
            rightPressed = true;
        } else if (e.keyCode == 37) {
            leftPressed = true;
        }
    }

    function keyUpHandler(e) {
        if (e.keyCode == 39) {
            rightPressed = false;
        } else if (e.keyCode == 37) {
            leftPressed = false;
        }
    }

    draw();
})();

